module Water
(
    Water,
    waterConstructor,

    bmkey,
    rotation,
    wet,
    source,
    level,
    from,

    drawWater,
    nextWaterLevel,
) where
    import Graphics.UI.WX
    import Graphics.UI.WXCore

    import Data.List
    import qualified Data.Map as Map

    import Settings



    data Water = Water {
        bmkey    :: String,
        rotation :: Int,
        bm       :: IO(Bitmap ()),
        wet      :: Bool,
        source   :: Bool,
        level    :: Int,
        from     :: [Int]
        }

    instance Eq Water
        where
            (==) w w' = level w == level w'



    waterConstructor :: (String,Int,Bool,Bool,Int,[Int]) -> Water
    waterConstructor (k,r,w,s,l,f) = Water {
        bmkey      = k,
        rotation   = r,
        bm         = loadBM k r f l,
        wet        = w,
        source     = s,
        level      = l,
        from       = f
        }



    nextWaterLevel :: Water -> Water
    nextWaterLevel w
        | l == 3 && length f == length k = waterConstructor (k,r,w',s,8  ,f)
        | l < 8  && w'                   = waterConstructor (k,r,w',s,l+1,f)
        | otherwise                      = w
        where
            k  = bmkey    w
            r  = rotation w
            w' = wet      w
            s  = source   w
            l  = level    w
            f  = from     w



    drawWater :: Water -> Point -> DC a -> IO ()
    drawWater w pt dc = bm w >>= \bm' -> drawBitmap dc bm' pt True []

    loadBM :: String -> Int -> [Int] -> Int -> IO (Bitmap ())
    loadBM k r f l =img >>= imageConvertToBitmap
        where
            img  = rotate r $ image path
            fk   = fromToKey f r
            path = imageDir++"water/"++k++"/"++fk++"_"++show l++".png"

    fromToKey :: [Int] -> Int -> String
    fromToKey fs r = concatMap show $ sort $ map (\f -> mod (f-r) 4) fs

    rotate :: Int -> Image () -> IO(Image ())
    rotate 0 img = return img
    rotate r img = imageRotate90 img False >>= rotate (r-1)

    getPaths :: String -> Int -> Bool -> [[Int]]
    getPaths x r s = if s then [[0]]
                          else combineOuter $ findIndices id
                                            $ map (`elem` x) "0123"



    combineOuter :: [Int] -> [[Int]]
    combineOuter []     = []
    combineOuter (x:xs) = combineInner [x] xs++combineOuter xs

    combineInner :: [Int] -> [Int] -> [[Int]]
    combineInner x []     = [x]
    combineInner x (y:ys) = combineInner x ys++combineInner (x++[y]) ys
