module GameState
(
    GameState
    (
        Prelude,
        Running,
        Paused,
        Victory,
        Loss
    ),
    tilesLeft,

    startGame,
    checkVictory,

    ifRunning,

    showState,

    decreaseTilesLeft,
    pauseToggle,
    leak
) where
    import Graphics.UI.WX

    import Data.List

    import Settings

    bmLoss    = bitmap $ imageDir++"misc/loss.png"
    bmVictory = bitmap $ imageDir++"misc/victory.png"

    data GameState = Prelude
                    {
                    tilesLeft :: Int
                    }
                   | Running
                    {
                    tilesLeft :: Int
                    }
                   | Paused
                    {
                    tilesLeft :: Int
                    }
                   | Victory
                    {
                    tilesLeft :: Int
                    }
                   | Loss
                    {
                    tilesLeft :: Int,
                    leaks     :: [(Int,Int,[Int])]
                    }
                   deriving(Eq)

    prelude :: Int -> GameState
    prelude tL = Prelude {tilesLeft = tL}

    running :: Int -> GameState
    running tL = Running {tilesLeft = tL}

    paused :: Int -> GameState
    paused tL  = Paused {tilesLeft = tL}

    victory :: GameState
    victory    = Victory  {tilesLeft = 0}

    loss :: Int -> [(Int,Int,[Int])] -> GameState
    loss tL ls = Loss {tilesLeft = tL, leaks = ls}



    ifRunning :: Var GameState -> IO() -> IO()
    ifRunning vGS command = varGet vGS >>= \gs -> case gs of
                               Running {} -> command
                               _          -> return()



    showState :: Settings -> Var GameState -> DC a -> IO()
    showState s vGS dc = ifLossVictory vGS
        (drawBitmap dc bmLoss    pt True [])
        (drawBitmap dc bmVictory pt True [])
        where
            pt = Point (div ((sidePanelW+tileW*fst (tileDims s))-64) 2)
                       (div ((     tileW+tileW*snd (tileDims s))-96) 2)

    ifLossVictory :: Var GameState -> IO() -> IO() -> IO()
    ifLossVictory vGS commandLoss commandVictory = varGet vGS >>= \gs ->
        case gs of
            Loss    {} -> commandLoss
            Victory {} -> commandVictory
            _          -> return()



    startGame :: Var GameState -> IO()
    startGame vGS   = varGet vGS >>= \gs -> case gs of
                      Prelude {} -> varSet vGS $ running $ tilesLeft gs
                      _          -> return ()

    pauseToggle :: Var GameState -> IO()
    pauseToggle vGS = varGet vGS >>= \gs -> case gs of
                      Running {} -> varSet vGS $ paused  $ tilesLeft gs
                      Paused  {} -> varSet vGS $ running $ tilesLeft gs
                      _          -> return ()

    checkVictory :: Var GameState -> Bool -> IO ()
    checkVictory _   True  = return()
    checkVictory vGS False = varGet vGS >>= \gs ->
                             if tilesLeft gs > 0
                                 then varSet vGS $ loss (tilesLeft gs) []
                                 else varSet vGS   victory



    decreaseTilesLeft :: Var GameState -> IO()
    decreaseTilesLeft vGS = varGet vGS >>= \gs -> when (tilesLeft gs > 0) $
                            varSet vGS $ running $ tilesLeft gs-1


    leak :: Var GameState -> (Int,Int) -> Int -> IO ()
    leak vGS (i,j) f = varGet vGS >>= \gs -> varSet vGS
                                             $ addLeak gs (i,j,f)
                                             $ getOldLeaks gs

    getOldLeaks :: GameState -> [(Int,Int,[Int])]
    getOldLeaks Running {} = []
    getOldLeaks Paused  {} = []
    getOldLeaks Victory {} = []
    getOldLeaks gs         = leaks gs

    addLeak :: GameState -> (Int,Int,Int) -> [(Int,Int,[Int])] -> GameState
    addLeak gs newLeak oldLeaks = loss (tilesLeft gs)
                                       (mergeLeak newLeak oldLeaks)

    mergeLeak ::  (Int,Int, Int) -> [(Int,Int,[Int])] -> [(Int,Int,[Int])]
    mergeLeak (x,y,f) []     = [(x,y,[f])]
    mergeLeak (x,y,f) ((x',y',fs):ls)
        | x == x' && y == y' = (x,y,sort $ f:fs) : ls
        | otherwise          = (x',y',[f])       : mergeLeak (x,y,f) ls
